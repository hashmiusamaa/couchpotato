<?php
require 'core.inc.php';
require 'connect.inc.php';

if(loggedin()){
	echo 'You\'re logged in as '.getuserfield("username").' <a href="logout.php">Log Out</a>';
	?>


    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery.js"></script>
	<script src="js/chat.js"></script>
<style>
	.chat{
		font: 0.8em Arial, Helvetica, sans-serif;	
	}
	.chat .messages{
		border: 1px solid black;
		width: 95%;
		height: 590px;
		padding: 10px;
		overflow-y: auto;
	}
	.chat .message a{
		color: slategrey;
	}
	.chat .message p{
		margin: 5px 0;
	}
	.chat .entry{
		width:100%;
		height: 40px;
		padding: 5px;
		margin-top: 5px;
		font: 1em Arial, Helvetica, sans-serif;
		
	}
</style>

	<div class="chat">
    	<div class="messages"></div>
       <center> <textarea class="entry" placeholder="Type here and enter to send message"></textarea></center>
    </div>
    <?php
}
else{
	include 'loginform.php';
}
?>
